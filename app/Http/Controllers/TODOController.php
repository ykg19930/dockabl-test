<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\Todo;
use Redirect;

class TODOController extends Controller
{
    public function __construct(Request $request){

    }
    public function submitTodo(Request $request){
        // dd($name);
        if($request->has('Name') && ($request->input('Name') !== NULL)){
            
            $data = new Todo;
            $data->name = $request->input('Name');
            $data->is_deleted = 0;
            $data->save();

            return Redirect::back()->with('success', 'Successfully Submitted');
        }else{
            return Redirect::back()->with('error', 'Please enter the name of the task');
        }
    }

    public function listTodo(){
        $lists = Todo::select('name','id')->where('is_deleted',0)->get();
        return view('/listTODO')->with('lists', $lists);
    }

    public function markTodo(){
        $lists = Todo::select('name','id')->where('is_deleted',0)->get();
        return view('/markTODO')->with('lists', $lists);
    }

    public function deleteTodo(Request $request){
        if($request->has('id')){
            $id = $request->input('id');
            $checkItem = Todo::find($id);
            if($checkItem){
                // $res=Todo::where('id',$id)->delete();
                $checkItem->is_deleted = 1;
                $checkItem->save();
                
                return Redirect::to('/marktodo')->with('success', 'Successfully Deleted');
            }else{
                return Redirect::to('/marktodo')->with('error', 'Invalid Input Provided');
            }
        }else{
            return Redirect::to('/marktodo')->with('error', 'Please select an item');
        }
    }
}
