<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $fillable = [
        'name', 'is_deleted'
    ];
}
