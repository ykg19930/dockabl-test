@include('welcome')
<div class="row" style="margin:20px">
    <h3>List of all TODOS</h3>    
</div>
@if(!$lists->isEmpty())
      <!-- $data is not empty -->
     <div class="row" style="margin:20px">
        <table class="table table-responsive">
            <thead class="thead-dark">
                <tr>
                <th scope="col">#</th>
                <th scope="col">Name of the Task</th>
                </tr>
            </thead>
            <tbody>
                <?php $count =1;?>
                @foreach($lists as $list)
                <tr>
                    <th scope="row">{{$count}}</th>
                    <td>{{$list->name}}</td>
                </tr>
                <?php $count++; ?>
                @endforeach
            </tbody>
        </table>
    </div>
@else
    <h4 style="margin:25px">No Data Available</h4>
@endif
    