@include('welcome')
<div class="row" style="margin:20px">
    <h3>All TODOS</h3>    
</div>
@include('layout.alertMessage')
@if(!$lists->isEmpty())
      <!-- $data is not empty -->
     <div class="row" style="margin:20px">
        <table class="table table-responsive">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name of the Task</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php $count =1;?>
                @foreach($lists as $list)
                <tr>
                    <th scope="row">{{$count}}</th>
                    <td>{{$list->name}}</td>
                    <th scope="row"><a class="deleteBtn" href="deletetodo?id={{$list->id}}"><button type="submit">Delete</button></a></th>
                </tr>
                <?php $count++; ?>
                @endforeach
            </tbody>
        </table>
    </div>
@else
    <h4 style="margin:25px">No Data Available</h4>
@endif