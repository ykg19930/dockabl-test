@if (\Session::has('error'))
    <div class="alert alert-danger" style="color:red;padding:0 px !important;">
        <ul style="margin:0px !important">
            <li>{!! \Session::get('error') !!}</li>
        </ul>
    </div>
@endif
@if (\Session::has('success'))
    <div class="alert alert-success" style="color:green;padding:0 px !important;;">
        <ul style="margin:0px !important">
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif