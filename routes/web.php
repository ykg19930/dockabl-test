<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/addtodo',function(){
    return view('addTODO');
});
Route::POST('/submitTODO','TODOController@submitTodo');
Route::GET('/listtodo','TODOController@listTodo');
Route::GET('/marktodo','TODOController@markTodo');
Route::GET('/deletetodo','TODOController@deleteTodo');
